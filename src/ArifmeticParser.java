import org.mariuszgromada.math.mxparser.Expression;
import java.io.*;

public class ArifmeticParser {

    public static void main(String[] args) throws IOException {
        System.out.println("Укажите путь к файлу");
        BufferedReader conReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = conReader.readLine();
        conReader.close();
        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));

        while (fileReader.ready()) {
            String s = fileReader.readLine();
            Expression expression = new Expression(s);
            double v = expression.calculate();
            fileWriter.write(v+"");
        }


        fileReader.close();
        fileWriter.close();
    }
}
